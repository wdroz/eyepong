@echo off

%1thrift.exe -r --gen csharp -o %1 %1*.thrift
%1thrift.exe -r --gen py -o %1 %1*.thrift
%1thrift.exe -r --gen cpp -o %1 %1*.thrift