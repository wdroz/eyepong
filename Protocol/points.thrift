namespace csharp ch.hesso.useinf.points
namespace py ch.hesso.useinf.points

struct Point
{
	1:i32 x,
	2:i32 y,
}

service PointsManager
{
    i32 ping(),
    Point getCurrentPoint(),
}
