# -*-coding:utf-8 -*
'''
Created on 5 avr. 2014

@author: Rapha�l
'''

from random import Random

from thrift.protocol.TBinaryProtocol import TBinaryProtocolFactory
from thrift.server import TServer
from thrift.transport.TSocket import TServerSocket
from thrift.transport.TTransport import TBufferedTransportFactory
from thrift.transport.TTransport import TFileObjectTransport
from ch.hesso.useinf.points import PointsManager
from ch.hesso.useinf.points.ttypes import *

class PointsHandler:
    def getCurrentPoint(self):
        print "beginning of function"
        r = Random()
        r.seed(None)
        p = Point( r.randint(10,100),  r.randint(10,100))
        print "%s : %s is going to be send" % (p.x, p.y)
        return p

    def ping(self):
        print "ping"
        return 100

if __name__ == '__main__':
    
    handler = PointsHandler()
    processor = PointsManager.Processor(handler)
    transport = TServerSocket(port=55555)
    tfactory = TBufferedTransportFactory()
    pfactory = TBinaryProtocolFactory()
    
    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    
    print 'Starting the fake captor service on %s' % transport.port
    server.serve()
    print 'done.'