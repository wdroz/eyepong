﻿using ch.hesso.useinf.points;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thrift;
using Thrift.Protocol;
using Thrift.Transport;

namespace testConsole
{
    class Program
    {
        
        static void Main(string[] args)
        {
            PointsManager.Client m_captor;
            TTransport m_transport;
            try
            {
                m_transport = new TSocket("localhost", 55555);
                TProtocol protocol = new TBinaryProtocol(m_transport);
                m_captor = new PointsManager.Client(protocol);


                m_transport.Open();
                if (m_transport.IsOpen)
                {
                    System.Console.WriteLine("the interface is connected to the captor");
                 
                }


                System.Console.WriteLine("avant ping");
                var ping = m_captor.ping();
                System.Console.WriteLine("apres ping : " + ping.ToString());

                var point = m_captor.getCurrentPoint();

                String toto = (point.X.ToString() + " : " + point.Y.ToString());
                System.Console.WriteLine(toto);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            System.Console.ReadKey();
        }
    }
}
