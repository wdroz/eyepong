﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thrift;
using Thrift.Transport;
using Thrift.Server;
using ch.hesso.useinf.points;

namespace Captor
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                PointsHandler handler = new PointsHandler();
                PointsManager.Processor processor = new PointsManager.Processor(handler);
                TServerTransport serverTransport = new TServerSocket(9090);
                TServer server = new TSimpleServer(processor, serverTransport);

                // Use this for a multithreaded server
                // server = new TThreadPoolServer(processor, serverTransport);

                Console.WriteLine("Starting the captor service...");

                server.Serve();
            }
            catch(TException x)
            {
                Console.WriteLine(x.Message);

            }
            catch (Exception x)
            {
                Console.WriteLine(x.StackTrace);
            }
            Console.ReadKey();
            Console.WriteLine("done.");
        }
    }
}
