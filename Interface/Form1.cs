﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Thrift;
using Thrift.Transport;
using Thrift.Protocol;
using ch.hesso.useinf.points;

namespace Interface
{
    public partial class Form1 : Form
    {
        PointsManager.Client m_captor;
        TTransport m_transport;
        public Form1()
        {
            try
            {
                m_transport = new TSocket("localhost", 55555);
                TProtocol protocol = new TBinaryProtocol(m_transport);
                m_captor = new PointsManager.Client(protocol);
               
            }
            catch (TException e)
            {
                System.Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            InitializeComponent();
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            try
            {
                
                System.Console.WriteLine("avant ping");
                var ping = m_captor.ping();
                System.Console.WriteLine("apres ping : "+ping.ToString());

                var point = m_captor.getCurrentPoint();
                this.labelTest.Text = (point.X.ToString() + " : " + point.Y.ToString());
                

            }
            catch(Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void buttonConnection_Click(object sender, EventArgs e)
        {
            try
            {
                m_transport.Open();
                if (m_transport.IsOpen)
                {
                    System.Console.WriteLine("the interface is connected to the captor");
                    MessageBox.Show("the interface is connected to the captor");
                }
            }
            catch(Exception x)
            {
            System.Console.WriteLine(x.Message);
            }
            
        }
    }
}
